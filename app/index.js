import Node from "../src";

export default () => async () => {
  const node = await Node({
    url: "https://html5.gamedistribution.com/bac58a7d348b4ef988c4eccd61b76ad4/#/",
    callback: (data) => {
      console.log(data);
    }
  });

  node.open();
}