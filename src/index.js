// iframe-dialog.js

/**
 * Creates a full-screen iframe for the given URL and provides open, close, and destroy methods.
 * The iframe can be closed by pressing the escape key or clicking the close button.
 * It also handles messages from the iframe and passes them to a callback.
 *
 * @param {Object} args Object containing the arguments.
 * @param {string} args.url The URL to be loaded inside the iframe.
 * @param {Function} args.callback The callback function to handle messages from the iframe.
 * @returns {Object} An object containing open, close, and destroy methods.
 */
export default async ({ url, callback }) => {
  // Create elements
  const overlay = document.createElement('div');
  const iframe = document.createElement('iframe');
  const closeButton = document.createElement('button');

  // Configure the overlay
  overlay.style.position = 'fixed';
  overlay.style.top = '0';
  overlay.style.left = '0';
  overlay.style.width = '100vw';
  overlay.style.height = '100vh';
  overlay.style.backgroundColor = 'rgba(0, 0, 0, 0.7)';
  overlay.style.zIndex = '1000';
  overlay.style.display = 'flex';
  overlay.style.justifyContent = 'center';
  overlay.style.alignItems = 'center';
  overlay.style.display = 'none';

  // Configure the iframe
  iframe.style.width = '100%';
  iframe.style.height = '100%';
  iframe.src = url;

  // Configure the close button
  closeButton.style.position = 'absolute';
  closeButton.style.top = '0px';
  closeButton.style.right = '0px';
  closeButton.style.border = 'none';
  closeButton.style.background = 'black';
  closeButton.style.color = 'white';
  closeButton.style.fontSize = '24px';
  // closeButton.style.borderRadius = '50%';
  closeButton.style.width = '30px';
  closeButton.style.height = '30px';
  closeButton.style.display = 'flex';
  closeButton.style.justifyContent = 'center';
  closeButton.style.alignItems = 'center';
  closeButton.innerHTML = '✖';

  // Append elements
  overlay.appendChild(iframe);
  overlay.appendChild(closeButton);
  document.body.appendChild(overlay);

  // Event listener for messages from the iframe
  const messageHandler = (event) => {
    if (event.origin !== new URL(url).origin) {
      // Ignore messages from different origins
      return;
    }
    callback(event.data);
  };

  // Event listener for the escape key
  const escapeHandler = (event) => {
    if (event.key === 'Escape' || event.keyCode === 27) {
      close();
    }
  };

  // Open the iframe
  const open = () => {
    overlay.style.display = 'flex';
    window.addEventListener('message', messageHandler);
    document.addEventListener('keydown', escapeHandler);
  };

  // Close the iframe
  const close = () => {
    overlay.style.display = 'none';
    window.removeEventListener('message', messageHandler);
    document.removeEventListener('keydown', escapeHandler);
  };

  // Assign the close function to the click event of the closeButton after its declaration
  closeButton.onclick = close;

  // Completely remove the iframe and related elements
  const destroy = () => {
    window.removeEventListener('message', messageHandler);
    document.removeEventListener('keydown', escapeHandler);
    document.body.removeChild(overlay);
  };
  
  return { open, close, destroy };
};